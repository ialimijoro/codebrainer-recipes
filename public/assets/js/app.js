var allRecipes;
var currentRecipe = null;
var forPeople;
var imageFolder = "assets/images/";

function limitRecipeDescription(description) {
  var length = 45;
  if (description.length > length) {
    return description.substring(0, length) + "...";
  }
  return description;
}

function fillSideNavMenu() {
  var sideNavUl = document.querySelector("#recipes-nav");
  var html = "";

  Object.keys(allRecipes).forEach(function(recipeId) {
    html += `<li class="card recipeNav__item" target="${
      allRecipes[recipeId].id
    }" id="card-${allRecipes[recipeId].id}">
      <img src="assets/images/${
        allRecipes[recipeId].thumb_img
      }" alt="" class="card-thumb">
      <div class="card-content">
        <h2 class="card-title">${allRecipes[recipeId].title}</h2>
        <p class="card-description">${limitRecipeDescription(
          allRecipes[recipeId].description
        )}</p>
      </div>
      <div class="card-footer">
        <small>${allRecipes[recipeId].for_people} people</small>
      </div>
    </li>`;
  });
  sideNavUl.innerHTML = html;

  document.querySelectorAll(".recipeNav__item").forEach(function(element) {
    element.addEventListener("click", function() {
      onRecipeClick(element);
    });
  });
}

function getRecipeFullImage(recipe) {
  if (recipe.full_img) {
    return imageFolder + recipe.full_img;
  }
  return imageFolder + recipe.thumb_img;
}

function getIngredientItem(recipePeopleNumber, ingredient, targetPeople = 0) {
  if (targetPeople && targetPeople != 0) {
    var calculatedQuantity =
      (ingredient.quantity * targetPeople) / recipePeopleNumber;
    return (
      calculatedQuantity.toFixed(2) +
      " " +
      ingredient.unit +
      " " +
      ingredient.name
    );
  }
  return ingredient.quantity + " " + ingredient.unit + " " + ingredient.name;
}

function listRecipeIngredients(recipe) {
  var recipeIngredients = recipe.ingredients;
  var recipeForPeople = recipe.for_people;

  var html = "";
  recipeIngredients.forEach(function(ingredient) {
    html += `<li> ${getIngredientItem(
      recipeForPeople,
      ingredient,
      forPeople
    )}</li>`;
  });
  return html;
}

function sortInstructions(instructions) {
  instructions.sort(function(a, b) {
    var orderA = a.order;
    var orderB = b.order;
    if (orderA < orderB) {
      return -1;
    }
    if (orderA > orderB) {
      return 1;
    }
    return 0;
  });
}

function listRecipeInstruction(recipeInstructions) {
  var html = "";
  sortInstructions(recipeInstructions);
  recipeInstructions.forEach(function(instruction) {
    html += `<li>${instruction.description}</li>`;
  });
  return html;
}

function getRecipePeople() {
  if (forPeople && !isNaN(forPeople) && forPeople > 0) {
    return forPeople;
  }
  return currentRecipe.for_people;
}

function onRecipeClick(recipeCard) {
  forPeople = 0;
  currentRecipe = allRecipes[recipeCard.getAttribute("target")];
  fillRecipeDetail();
}

function fillRecipeDetail() {
  if (currentRecipe == null) {
    currentRecipe = allRecipes[Object.keys(allRecipes)[0]];
  }

  var activeRecipe = document.querySelector(".card--active");
  if (activeRecipe) {
    activeRecipe.classList.remove("card--active");
  }

  newActiveRecipe = document.querySelector("#card-" + currentRecipe.id);
  newActiveRecipe.classList.add("card--active");

  document.querySelector("#recipeTitle").innerHTML = `${currentRecipe.title}`;
  document
    .querySelector("#recipeFullImage")
    .setAttribute("src", getRecipeFullImage(currentRecipe));
  document.querySelector("#recipeDescriptionText").innerHTML =
    currentRecipe.description;
  document.querySelector("#recipeForPeople").innerHTML = getRecipePeople();
  document.querySelector(
    "#recipeIngredientLists"
  ).innerHTML = listRecipeIngredients(currentRecipe);
  document.querySelector(
    "#listRecipeIngredients"
  ).innerHTML = listRecipeInstruction(currentRecipe.instructions);
}

function onClickcalculateIngredient() {
  var forPeopleInput = document.querySelector("#for-people-input").value;

  if (forPeopleInput == "" && isNaN(forPeople) && forPeopleInput == 0) {
    alert("Please enter a valid number.");
  } else {
    forPeople = forPeopleInput;
  }

  fillRecipeDetail();
}

function getRecipes() {
  fetch("assets/data/recipes.json")
    .then(function(response) {
      return response.json();
    })
    .then(function(jsonData) {
      allRecipes = jsonData.recipes;
      fillSideNavMenu();
      fillRecipeDetail();
    })
    .catch(function(error) {
      console.error(error);
    });
}

function init() {
  getRecipes();
}

function loadPageSection(url, selector, callback) {
  fetch(url)
    .then(function(response) {
      return response.text();
    })
    .then(function(jsonData) {
      console.log(jsonData)
      document.querySelector(selector).innerHTML = jsonData;
      callback();
    })
    .catch(function(error) {
      console.error(error);
    });
}

document.addEventListener("DOMContentLoaded", function() {
  loadPageSection("template/recipe-detail.html", "#recipeDetail", function() {
    document
      .querySelector("#calculate-btn")
      .addEventListener("click", onClickcalculateIngredient);
    init();
  });
});
